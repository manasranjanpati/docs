# Requirement for *Keyword Search* | *Popular Keywords*
------------------------------------------

![](/docs/Video-20210210_050423-Meeting Recording.mp4)
#### Keyword Search :
* Keyword search will be a Module where user will search with single keywords (ireespective of website,domain ) with another filter i.e `Country` with Superadmin functionality where the Admin can decrease or increase the list of countries to filter once(10 min).
* Need to show same UI after filter with small design changes like `Status(We need to % of completion like 100%)`. We have another functionality to add for Keyword Search i.e  **ADD TO LIST** by selecting no of checkboxes and **EXPORT** (Can follow MyList module where we have same kind of functionality expect Alerts and Select a list)
* If User will do both operation **ADD TO LIST** and **EXPORT** we will consider that user is viewing alll the contacts he selected so we need to pop one alert EX: ***Adding 620 contacts means you are viewing those contacts/loosing credits***
* Same things have to Implemented in Before Login Page

![Leadsolo](/docs/keyword_lookup.png)


#### Popular Keywords
* By selecting any Popular key user will redirect to that specific keyword page(In the same page)
> Popular Keywords(Page with Multiple Keywords) --> Popular Keywords/Fashion (Data belongs to Fashion Keyword)
* In Location we will show only countries with multiple selection for user.

![Leadsolo1](/docs/popular_keywords.png)

* The UI We will use are below with small design changes.

![Leadsolo2](/docs/popular_keywords_next_page.png)
